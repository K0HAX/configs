(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(gnutls-trustfiles
   '("/etc/ssl/certs/ca-certificates.crt" "/etc/pki/tls/certs/ca-bundle.crt" "/etc/ssl/ca-bundle.pem" "/usr/ssl/certs/ca-bundle.crt" "/usr/local/share/certs/ca-root-nss.crt" "/etc/ssl/cert.pem" "/etc/certs/ca-certificates.crt" "~/.emacs.d/gnus-certs/protonmail.pem" "~/.emacs.d/gnus-certs/protonmail-new.pem"))
 '(nnmail-expiry-wait 1)
 '(package-selected-packages
   '(ement ob-ledger yaml-mode yaml ansible restart-emacs ob-blockdiag ox-gfm ox-mediawiki ox-twbs org-bullets company-graphviz-dot graphviz-dot-mode cmake-mode blockdiag-mode markdown-mode go-eldoc go-complete go-stacktracer go-mode vagrant helm-emms helm-pass helm-bbdb helm-spotify-plus govc company exec-path-from-shell web-mode multi-term w3m emms-info-mediainfo emms json-mode smex ledger-mode magit-popup magit powerline-evil powerline erc-youtube erc-image erc-hl-nicks erc-crypt erc-colorize git request bbdb dtrt-indent htmlize rainbow-delimiters s use-package evil-org evil-leader))
 '(ps-always-build-face-reference t)
 '(ps-print-color-p nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(trailing-whitespace ((t (:background "dim gray")))))
